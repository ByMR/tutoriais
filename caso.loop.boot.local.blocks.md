# Caso loop local-block


## História

Seguindo o tutorial em vídeo do Prof. Paulo Kretcheu, [Curso GNU Linux - Tutorial sobre transplante de instalação - Debian Day 2020](https://www.youtube.com/watch?v=_UIQnOuJ3lA), realizei a instalção do debian 10 em um host também com debian 10 com o libvirt. 

A instalação ocoreu como esperado, mas o transplante foi digamos 98%.

#### Configuração da máquina virtual doadora

1. Sistema UEFI
2. vda - disco usado com hdd interno que receberá a instalação do debian.
    * vda1 configurado para partição EFI.
    * vda2 configurado para partição boot.
    * vda3 configurado para swap.
    * vda4 configurado para o / (barra).
3. vdb para ser usado como unidade externa.

#### Configuração da máquina virtual receptora

1. Sistema BIOS :p
2. vda - também usado com hdd interno.
    * vda1 para partição EFI.
    * vda2 para partição boot.
    * vda3 para partição swap.
    * vda4 para partição / (barra).
    * vda5 para partição home.
3. vdb - o mesmo utilizado na vm doadora.

Como já devem ter percebido há uma pequena diferença nas vm que me fez falhar na primeira tentativa de inicialização da vm receptora. Ou seja os 1% foi justamente a vm receptora ser baseada em BIOS.

Em principio gostei do ocorrido pois me tirava do script da vídeo aula e ali me era apresentado um desafio.

## Resolução 1%

O primeiro boot como dito antes falhou :/. A velha tela da bios reclamando que não tem um dispositivo do boot ~~puts onde errei~~, então bootar o live-cd debian pesquisando na wiki do debian achei a página [GrubEFIReinstall](https://wiki.debian.org/GrubEFIReinstall?highlight=%28reinstall%29%7C%28grub%29)

Ao executar o comando:
```shell
 [ -d /sys/firmware/efi ] && echo "EFI boot on HDD" || echo "Legacy boot on HDD" 
 ```

tive a saída: 
`` Legacy boot on HDD ``
Nesse momento percebi que estava com o sistema "em bios". Segui com a remoção do pacote **grub-efi-amd64**
```shell 
apt purge grub-efi-amd64 
```

> deixei o apt fazer seu trabalho e remover as dependências junto.

Realizei a instalação do grub para bios
```shell
 apt install grub-pc 
 ```
após um 
```shell
 grub-install --target=x86_64-efi /dev/vda1 
 ```

Lindo :) o sistema bootou. hum :/ mas está lento...

## Resolução +1%
Bom aqui vem o real intúito do meu relato (para que eu não esqueça é claro).

Primeiro alterei o arquivo de configuração padrão do grub
```shell
 vi /etc/default/grub 
 ```
Alterei a linha
``` GRUB_CMDLINE_LINUX_DEFAULT="quiet" ``` por ``` GRUB_CMDLINE_LINUX_DEFAULT=""```

então no boot vi a linha se repetir várias vezes: 
``` 
...
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
Begin: Running /scripts/local-block ... done.
...
 ```

 Claramente achei o que estava atrasando o inicio da máquina, mas que diabos é esse bicho aí?

 Numa *googlada* básica achei esse [vídeo](https://www.youtube.com/watch?v=w-XfOR4LDi0) no youtube. 
 > Eu sei, não é bem assim um vídeo, mas tá no youtube. A regra é clara.

 Daí foi rápido achar o UUID da partição swap com o **blkid** alterar o arquivo ``` etc/initramfs-tools/conf.d/resume```
 e colocar o UUID da partição do sistem receptor. No arquivo *resume* é só alterar o UUID antigo pelo novo UUID da partição swap.
***
 # Finalizando

 A lentidão no boot da vm foi causada pelo erro na configuração da swap, apesar do sistema subir com a swap ativada ainda assim o sistema procurava a partição antiga. Porém com a opção *quiet* ativada na configuração padrão do grub me retornava a mensagem em tela.
 ``` Gave up waiting for suspend/resume device ```