
# Tutoriais de Debian

Criei o fork desse repositório por achar uma ótima opção em documentar minhas experiências pessoas com Software Livre, seguindo o que o Professor [Kretcheu](salsa.debian.org/kretcheu/) vinha realizando.

## Em seguida o README original
***

- Para tirar dúvidas sobre Debian, **Sobre Debian**

   - No telegram:

   - [Grupo Debian Brasil](https://t.me/debianbrasil)

   - [Grupo Debian BR](https://t.me/debianbr)


- Para me encontrar:

   - [Youtube](https://youtube.com/kretcheu2001)

   - Email: [kretcheu@gmail.com](mailto:kretcheu@gmail.com)

   - Nas mídias sociais @kretcheu

- Licença

   - Todo material aqui fornecido é licenciado sob a licença (CC BY-SA 4.0).\
<https://creativecommons.org/licenses/by-sa/4.0/>
